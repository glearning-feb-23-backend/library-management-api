package com.glearning.lms.service;

import java.util.Map;

import com.glearning.lms.model.Library;

public interface LibraryService {
	
	Library updateLibrary(long id, Library updatedLibrary);

	Library saveLibrary(Library library);
	
	Library addLibraryWithSaveAndFlush(Library library);
	
	Map<String, Object> fetchAllLibraries(int page, int size, String direction, String property);


	Library getLibraryById(long libraryId);

	void deleteLibraryById(long id);

}
