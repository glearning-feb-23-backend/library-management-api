package com.glearning.lms.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.glearning.lms.model.Library;
import com.glearning.lms.repository.LibraryRepository;

@Service
public class LibraryServiceImpl implements LibraryService {
	
	@Autowired
	private LibraryRepository libraryRepository;

	@Override
	public Library updateLibrary(long id, Library updatedLibrary) {
		return null;
	}

	@Override
	public Library saveLibrary(Library library) {
		return this.libraryRepository.save(library);
	}

	@Override
	public Library addLibraryWithSaveAndFlush(Library library) {
		return this.libraryRepository.save(library);
	}

	@Override
	public Library getLibraryById(long libraryId) {
		return this.libraryRepository.findById(libraryId).orElseThrow(() -> new IllegalArgumentException("invalid library id"));
	}

	@Override
	public void deleteLibraryById(long id) {
		this.libraryRepository.deleteById(id);
	}
	
	@Override
	public Map<String, Object> fetchAllLibraries(int page, int size, String strDirection, String property) {
		
		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		
		PageRequest pageRequest = PageRequest.of(page, size, direction, property);
		
		Page<Library> response = this.libraryRepository.findAll(pageRequest);
		
		
		int pages = response.getTotalPages();
		long count = response.getTotalElements();
		List<Library> content = response.getContent();
		
		Map<String, Object> responseMap = new LinkedHashMap<>();
		
		responseMap.put("pages", pages);
		responseMap.put("count", count);
		responseMap.put("content", content);
		return responseMap;
	}


}
