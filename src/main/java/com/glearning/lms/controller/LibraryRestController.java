package com.glearning.lms.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.lms.model.Library;
import com.glearning.lms.service.LibraryService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/library")
public class LibraryRestController {
	
	@Autowired	
	private LibraryService libraryService;
	
	@ApiOperation(value = "fetch all the Libraries", notes = "Fetches all the libraries in the library table")
	@GetMapping
	public Map<String, Object> fetchAllLibraries(
			@RequestParam(name="size", required=false, defaultValue="10") int size, 
			@RequestParam(name="page", required=false, defaultValue="0") int page, 
			@RequestParam(name="sort", required=false, defaultValue="asc") String direction, 
			@RequestParam(name="field", required=false, defaultValue="name") String property
			){
		
		return this.libraryService.fetchAllLibraries(page, size, direction, property);	
	}
	
	@ApiOperation(value = "fetch the Library by its id", notes = "Fetches the library with the id provided")
	@GetMapping("/{id}")
	public Library fetchLibraryById(@PathVariable("id") long libraryId) {
		return this.libraryService.getLibraryById(libraryId);
	}
	
	@ApiOperation(value = "save the Library", notes = "Persist the library")
	@PostMapping
	public void saveLibraryById(@RequestBody Library library) {
		this.libraryService.saveLibrary(library);
	}
	
	@PutMapping("/{id}")
	public void updateLibraryById(@PathVariable("id") long libraryId, Library updatedLibrary) {
		this.libraryService.updateLibrary(libraryId, updatedLibrary);
	}
	
	@DeleteMapping("/{id}")
	public void deleteLibraryById(@PathVariable("id") long libraryId) {
		this.libraryService.deleteLibraryById(libraryId);
	}

}
